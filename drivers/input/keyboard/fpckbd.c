/*
 * fpckbd.c
 *
 * Copyright 2020, Gary Wong <gtw@gnu.org>
 *
 * This file is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this file.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <linux/input.h>
#include <linux/io.h>
#include <linux/module.h>
#include <uapi/linux/input-event-codes.h>

MODULE_AUTHOR( "Gary Wong <gtw@gnu.org>" );
MODULE_DESCRIPTION( "FPC-III keyboard driver" );
MODULE_LICENSE( "GPL" );

#define FPCKBD_BASE 0xA0000000

static struct timer_list timer;
static struct input_dev *fpc_dev;
static unsigned char __iomem *report;
static unsigned char last_rep[ 8 ];

static const unsigned char code_trans[ 0x66 ] = {
    0, 0, 0, 0, KEY_A, KEY_B, KEY_C, KEY_D,
    KEY_E, KEY_F, KEY_G, KEY_H, KEY_I, KEY_J, KEY_K, KEY_L,
    KEY_M, KEY_N, KEY_O, KEY_P, KEY_Q, KEY_R, KEY_S, KEY_T,
    KEY_U, KEY_V, KEY_W, KEY_X, KEY_Y, KEY_Z, KEY_1, KEY_2,
    KEY_3, KEY_4, KEY_5, KEY_6, KEY_7, KEY_8, KEY_9, KEY_0,
    KEY_ENTER, KEY_ESC, KEY_BACKSPACE, KEY_TAB,
        KEY_SPACE, KEY_MINUS, KEY_EQUAL, KEY_LEFTBRACE,
    KEY_RIGHTBRACE, KEY_BACKSLASH, 0, KEY_SEMICOLON,
        KEY_APOSTROPHE, KEY_GRAVE, KEY_COMMA, KEY_DOT,
    KEY_SLASH, KEY_CAPSLOCK, KEY_F1, KEY_F2,
        KEY_F3, KEY_F4, KEY_F5, KEY_F6,
    KEY_F7, KEY_F8, KEY_F9, KEY_F10,
        KEY_F11, KEY_F12, KEY_SYSRQ, KEY_SCROLLLOCK,
    KEY_PAUSE, KEY_INSERT, KEY_HOME, KEY_PAGEUP,
    KEY_DELETE, KEY_END, KEY_PAGEDOWN, KEY_RIGHT,
    KEY_LEFT, KEY_DOWN, KEY_UP, KEY_NUMLOCK,
    KEY_KPSLASH, KEY_KPASTERISK, KEY_KPMINUS, KEY_KPPLUS,
    KEY_KPENTER, KEY_KP1, KEY_KP2, KEY_KP3,
    KEY_KP4, KEY_KP5, KEY_KP6, KEY_KP7,
    KEY_KP8, KEY_KP9, KEY_KP0, KEY_KPDOT,
    0, KEY_COMPOSE
};

static const unsigned char mod_trans[ 8 ] = {
    KEY_LEFTCTRL, KEY_LEFTSHIFT, KEY_LEFTALT, KEY_LEFTMETA,
    KEY_RIGHTCTRL, KEY_RIGHTSHIFT, KEY_RIGHTALT, KEY_RIGHTMETA    
};

static void fpckbd_timer( struct timer_list *t ) {

    int i, j;
    unsigned char new_rep[ 8 ];

    memcpy_fromio( new_rep, report, 8 );

    for( i = 0; i < 8; i++ )
	if( ( new_rep[ 0 ] ^ last_rep[ 0 ] ) & ( 1 << i ) )
	    input_report_key( fpc_dev, mod_trans[ i ],
			      new_rep[ 0 ] & ( 1 << i ) );
    
    for( i = 2; i < 8; i++ )
	if( last_rep[ i ] ) {
	    for( j = 2; j < 8; j++ )
		if( new_rep[ j ] == last_rep[ i ] )
		    goto ignore_old;

	    if( last_rep[ i ] < 0x66 && code_trans[ last_rep[ i ] ] )
		input_report_key( fpc_dev, code_trans[ last_rep[ i ] ], 0 );
	ignore_old:
	    ;
	}
    
    for( i = 2; i < 8; i++ )
	if( new_rep[ i ] ) {
	    for( j = 2; j < 8; j++ )
		if( last_rep[ j ] == new_rep[ i ] )
		    goto ignore_new;
	    
	    if( new_rep[ i ] < 0x66 && code_trans[ new_rep[ i ] ] )
		input_report_key( fpc_dev, code_trans[ new_rep[ i ] ], 1 );
	ignore_new:
	    ;
	}
    
    input_sync( fpc_dev );

    memcpy( last_rep, new_rep, 8 );

    mod_timer( &timer, jiffies + HZ / 100 + 1 );
}

static int __init fpckbd_init( void ) {

    int i, err;
    
    if( !( fpc_dev = input_allocate_device() ) )
	return -ENOMEM;

    // FIXME use platform device to get address
    if( !( report = ioremap( FPCKBD_BASE, 0x10000 ) ) )
	return -ENOMEM;
    
    fpc_dev->evbit[ 0 ] = BIT_MASK( EV_KEY ) | BIT_MASK( EV_REP );

    for( i = 1; i < 0x80; i++ )
	__set_bit( i, fpc_dev->keybit );

    if( ( err = input_register_device( fpc_dev ) ) )
	return err;
    
    timer_setup( &timer, fpckbd_timer, 0 );
    mod_timer( &timer, jiffies + HZ / 100 + 1 );

    return 0;
}

static void __exit fpckbd_exit( void ) {

    del_timer_sync( &timer );
    
    input_unregister_device( fpc_dev );

    // FIXME iounmap
}

module_init( fpckbd_init );
module_exit( fpckbd_exit );
