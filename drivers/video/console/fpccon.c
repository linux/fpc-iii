/*
 * fpccon.c
 *
 * Copyright 2020, Gary Wong <gtw@gnu.org>
 *
 * This file is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This file is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this file.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <linux/console.h>
#include <linux/io.h>
#include <linux/module.h>
#include <linux/string.h>
#include <linux/vt_kern.h>

static u16 *fpc_videochar_base;

#define FPCCON_ROWS 28
#define FPCCON_COLS 100
#define FPCCON_MEMBASE 0x30000000
#define FPCCON_MEMSIZE 0x4000

#define VIDEOCHAR( y, x ) ( fpc_videochar_base[ ( (y) << 7 ) | (x) ] )

static const char *fpccon_startup( void ) {

    fpc_videochar_base = memremap( FPCCON_MEMBASE, FPCCON_MEMSIZE,
				   MEMREMAP_WB );
    
    return "FPC";
}

static void fpccon_init( struct vc_data *c, int init ) {

    c->vc_cols = FPCCON_COLS;
    c->vc_rows = FPCCON_ROWS;
    c->vc_can_do_color = 1;
    c->vc_complement_mask = 0x7700;
    c->vc_hi_font_mask = 0;
}

static void fpccon_deinit( struct vc_data *c ) {
}

static void fpccon_clear( struct vc_data *c, int y, int x,
			  int height, int width ) {
    int row, col;

    for( row = y; row < y + height; row++ )
	for( col = x; col < x + width; col++ )
	    VIDEOCHAR( row, col ) = c->vc_video_erase_char;
}
                        
static void fpccon_putc( struct vc_data *c, int ch, int y, int x ) {

    VIDEOCHAR( y, x ) = ( c->vc_attr << 8 ) | ch;
}

static void fpccon_putcs( struct vc_data *c, const unsigned short *s,
			  int count, int y, int x ) {
    
    u16 *p = &VIDEOCHAR( y, x );
    
    while( count ) {
	*p++ = ( c->vc_attr << 8 ) | ( *s++ & 0xFF );
	count--;
    }
}

static void fpccon_cursor( struct vc_data *c, int mode ) {
}

static bool fpccon_scroll( struct vc_data *c, unsigned int top,
			   unsigned int bottom, enum con_scroll dir,
			   unsigned int lines ) {
    int y;
    
    if( dir == SM_UP ) {
	for( y = top; y < bottom - lines; y++ )
	    memcpy( &VIDEOCHAR( y, 0 ), &VIDEOCHAR( y + lines, 0 ),
		    c->vc_cols << 1 );
	for( ; y < bottom; y++ )
	    memset32( (uint32_t *) &VIDEOCHAR( y, 0 ),
		      ( c->vc_video_erase_char << 16 ) |
		      c->vc_video_erase_char, c->vc_cols >> 1 );
    } else {
	for( y = bottom - 1; y >= top + lines; y-- )
	    memcpy( &VIDEOCHAR( y, 0 ), &VIDEOCHAR( y - lines, 0 ),
		    c->vc_cols << 1 );
	for( ; y >= top; y-- )
	    memset32( (uint32_t *) &VIDEOCHAR( y, 0 ),
		      ( c->vc_video_erase_char << 16 ) |
		      c->vc_video_erase_char, c->vc_cols >> 1 );
    }
    
    return 0;
}

static int fpccon_switch( struct vc_data *c ) {

    return 1; /* need redraw */
}

static int fpccon_blank( struct vc_data *c, int blank, int mode_switch ) {

    return 0; /* should probably add hardware support for blanking */
}

const struct consw fpc_con = {
    .owner = THIS_MODULE,
    .con_startup = fpccon_startup,
    .con_init = fpccon_init,
    .con_deinit = fpccon_deinit,
    .con_clear = fpccon_clear,
    .con_putc = fpccon_putc,
    .con_putcs = fpccon_putcs,
    .con_cursor = fpccon_cursor,
    .con_scroll = fpccon_scroll,
    .con_switch = fpccon_switch,
    .con_blank = fpccon_blank
};
EXPORT_SYMBOL( fpc_con );

MODULE_LICENSE( "GPL" );
